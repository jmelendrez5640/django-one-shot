from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem
from django.shortcuts import redirect
from todos.forms import TodoForm, ItemForm
# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "show_list": todo_detail
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm(instance=todo_list)
        context = {
            "form": form
        }
        return render(request, "todos/update.html", context)


def delete_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = ItemForm()
    return render(request, "todos/createitem.html", {"form": form})


def update_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todo_item.list.id)
    else:
        form = ItemForm(instance=todo_item)
        context = {
            "form": form
        }
        return render(request, "todos/updateitem.html", context)
